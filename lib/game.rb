require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  def initialize(p1, p2)
    @player_one = p1
    @player_two = p2
    @board = Board.new
    @current_player = p1
  end

  attr_accessor :player_one, :player_two, :board, :current_player


  def current_player
    @current_player
  end

  def switch_players!
    if @current_player == @player_one
      @current_player = @player_two
    else
      @current_player = @player_one
    end
  end

  def play_turn
    current_player.display(@board)
    move = current_player.get_move
    @board.place_mark(move, current_player.mark)

    switch_players!
  end

  def play
    until board.over?
      play_turn
    end
  end

end
