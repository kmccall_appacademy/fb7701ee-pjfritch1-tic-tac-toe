class HumanPlayer
  def initialize(name, mark = :X)
    @name = name
    @mark = mark
  end

  attr_accessor :mark, :name

  def display(board)
    p board
  end

  def get_move
    puts "where"
    move = gets.chomp
    pos = move.split(", ")
    pos.map! {|coord| coord.to_i}
  end
end
