class Board
  def initialize(grid = Array.new(3) {Array.new(3)})
    @grid = grid
  end

  attr_accessor :grid

  def place_mark(pos, mark)
    @@mark = mark
    @@y = pos[0]
    @@x = pos[1]
    raise "position already filled" unless empty?(pos)

    grid[@@y][@@x] = mark
  end

  def empty?(pos)
    return true if grid[@@y][@@x] == nil

    false
  end

  def winner
    return @@mark if horizontal_winner? || vertical_winner? || diagonal_winner?

    nil
  end

  def over?
    return true if winner != nil || cats_game?

    false
  end

  def horizontal_winner?
    grid[@@y].each {|sym| return false if sym != @@mark}

    true
  end

  def vertical_winner?
    grid.each {|row| return false if row[@@x] != @@mark}

    true
  end

  def diagonal_winner?
    left_diagonal = [grid[0][0],grid[1][1],grid[2][2]]
    right_diagonal = [grid[0][2],grid[1][1],grid[2][0]]
    right_winner = true
    left_winner = true
    has_winner = false

    left_diagonal.each {|el| left_winner = false if el != @@mark}
    right_diagonal.each {|el| right_winner = false if el != @@mark}

    has_winner = true if right_winner || left_winner
    has_winner
  end

  def cats_game?
    grid.each do |row|
      row.each do |el|
        return false if el.class != Symbol
      end
    end

    true
  end

end
