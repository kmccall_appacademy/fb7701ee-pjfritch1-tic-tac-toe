class ComputerPlayer
  def initialize(name, mark = :O)
    @name = name
    @mark = mark
  end

  attr_accessor :board, :mark

  def display(board)
    @board = board
  end

  def get_move
    if horizontal_winner != nil
      move = horizontal_winner
    elsif vertical_winner != nil
      move = vertical_winner
    elsif diagonal_winner != nil
      move = diagonal_winner
    else
      move = [rand(0..2),rand(0..2)]
    end
    move
  end

  private

  def horizontal_winner
    board.grid.each do |row|
      marks = 0
      row.each do |sym|
        if sym.class == Symbol && sym != @mark
          return nil
        elsif sym == @mark
          marks += 1
        end
        if marks == 2
          return [row,row.find_index {|sym| sym != @mark}]
        end
      end
    end
    nil
  end

  def vertical_winner
    transposed =board.grid.transpose
    transposed.each do |row|
      marks = 0
      row.each do |sym|
        if sym.class == Symbol && sym != @mark
          return nil
        elsif sym == @mark
          marks += 1
        end
        if marks == 2
          return [row.find_index {|sym| sym != @mark}, transposed.find_index(row)]
        end
      end
    end
    nil
  end

  def diagonal_winner
    left_diagonal = [board.grid[0][0],board.grid[1][1],board.grid[2][2]]
    right_diagonal = [board.grid[0][2],board.grid[1][1],board.grid[2][0]]
    left_marks = 0
    right_marks = 0

    if left_diagonal.find_all {|el| el == @mark}.count == 2 && left_diagonal.find {|el| el.class == Symbol && el != @mark} == nil
      row = left_diagonal.find_index {|el| el != @mark}
      return [row, row]
    elsif right_diagonal.find_all {|el| el == @mark}.count == 2 && right_diagonal.find {|el| el.class == Symbol && el != @mark} == nil
      row = right_diagonal.find_index {|el| el != @mark}
      return [row, 2 - row]
    end

    nil
  end
end
